﻿using Biblioteka.Data;
using Biblioteka.Models;
using Microsoft.EntityFrameworkCore;

namespace Biblioteka.Services.BookService
{
    public class BookService : IBookService
    {
        private readonly DataContext _context;
        private static readonly List<Book> books = new List<Book>();
        public BookService(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Book>> GetAllBooks()
        {
            return await _context.Books.ToListAsync();
        }
        public async Task<Book?> GetSingleBook(int id)
        {
            var book = await _context.Books.FindAsync(id);
            return book;
        }
        public async Task<List<Book>> AddBook(Book book)
        {
            _context.Books.Add(book);
            books.Add(book);
            await _context.SaveChangesAsync();
            return books;
        }
        public async Task<List<Book>?>  UpdateBook(int id, Book req)
        {
            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return null;
            }
            book.Autor = req.Autor;
            book.Naziv = req.Naziv;
            book.Zanr = req.Zanr;
            await _context.SaveChangesAsync();
            return books;
        }
        public async Task<bool> DeleteBook(int id)
        {
            var book = await _context.Books.FindAsync(id);
            if (book is null)
            {
                return false;
            }
            _context.Books.Remove(book);
            books.Remove(book);
            await _context.SaveChangesAsync();
            return true;
        }

    }
}
