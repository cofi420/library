﻿using Biblioteka.Models;
using Microsoft.AspNetCore.Mvc;

namespace Biblioteka.Services.BookService
{
    public interface IBookService
    {
        Task<List<Book>> GetAllBooks();
        Task<Book?> GetSingleBook(int id);
        Task<List<Book>> AddBook(Book book);
        Task<List<Book>?> UpdateBook(int id, Book req);
        Task<bool> DeleteBook(int id);
    }
}
