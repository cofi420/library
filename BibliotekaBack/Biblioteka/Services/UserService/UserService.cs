﻿using Biblioteka.Data;
using Biblioteka.Models;
using Microsoft.EntityFrameworkCore;

namespace Biblioteka.Services.UserService
{
    
    public class UserService : IUserService
    {
        private readonly DataContext _context;
        private static readonly List<User> users = new List<User>();
        public UserService(DataContext context)
        {
            _context = context;
        }
        public async Task<List<User>> GetAllUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User?> GetSingleUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            return user;
        }

        public async Task<List<User>> AddUser(User User)
        {
            _context.Users.Add(User);
            users.Add(User);
            await _context.SaveChangesAsync();
            return users;
        }

        public async Task<List<User>?> UpdateUser(int id, User req)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return null;
            }
            user.Name = req.Name;
            user.Password = req.Password;
            user.Email = req.Email;
            await _context.SaveChangesAsync();
            return users;
        }

        public async Task<bool> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user is null)
            {
                return false;
            }
            _context.Users.Remove(user);
            users.Remove(user);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
