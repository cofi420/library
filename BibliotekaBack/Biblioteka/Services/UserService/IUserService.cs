﻿using Biblioteka.Models;

namespace Biblioteka.Services.UserService
{
    public interface IUserService
    {
        Task<List<User>> GetAllUsers();
        Task<User?> GetSingleUser(int id);
        Task<List<User>> AddUser(User User);
        Task<List<User>?> UpdateUser(int id, User req);
        Task<bool> DeleteUser(int id);
    }
}
