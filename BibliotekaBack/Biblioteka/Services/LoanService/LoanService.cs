﻿using Biblioteka.Models;
using Biblioteka.Data;
using Microsoft.EntityFrameworkCore;

namespace Biblioteka.Services.LoanService
{
    public class LoanService : ILoanService
    {
        private DataContext _context;
        private readonly List<Loan> Loans = new List<Loan>();
        public LoanService(DataContext context)
        {
            _context = context;
        }
        public async Task<List<Loan>> GetAllLoans()
        {
            return await _context.Loans.ToListAsync();
        }
        public async Task<Loan?> GetSingleLoan(int id)
        {
            var loan = await _context.Loans.FindAsync(id);
            return loan;
        }
        public async Task<List<Loan>> AddLoan(Loan Loan)
        {
            _context.Loans.Add(Loan);
            Loans.Add(Loan);
            await _context.SaveChangesAsync();
            return Loans;
        }
        public async Task<List<Loan>?> UpdateLoan(int id, Loan req)
        {
            var Loan = await _context.Loans.FindAsync(id);
            if (Loan == null)
            {
                return null;
            }
            Loan.Book = req.Book;
            Loan.User = req.User;
            Loan.Start = req.Start;
            Loan.End = req.End;
            await _context.SaveChangesAsync();
            return Loans;
    }
        public async Task<bool> DeleteLoan(int id)
        {
            var Loan = await _context.Loans.FindAsync(id);
            if (Loan is null)
            {
                return false;
            }
            _context.Loans.Remove(Loan);
            Loans.Remove(Loan);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
