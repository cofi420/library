﻿using Biblioteka.Models;

namespace Biblioteka.Services.LoanService
{
    public interface ILoanService
    {
        Task<List<Loan>> GetAllLoans();
        Task<Loan?> GetSingleLoan(int id);
        Task<List<Loan>> AddLoan(Loan Loan);
        Task<List<Loan>?> UpdateLoan(int id, Loan req);
        Task<bool> DeleteLoan(int id);
    }
}
