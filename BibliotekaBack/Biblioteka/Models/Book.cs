﻿namespace Biblioteka.Models
{
    public class Book 
    {
        public int Id { get; set; }
        public string Naziv { get; set; } = string.Empty;
        public string Autor { get; set; } = string.Empty;
        public string Zanr { get; set; } = string.Empty;
        public Book()
        {

        }
        public Book(int id, string naziv, string autor, string zanr)
        {
            Id = id;
            Naziv = naziv;
            Autor = autor;
            Zanr = zanr;
        }
    }
}
