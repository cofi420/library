﻿namespace Biblioteka.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public User() { }
        public User(int id, string name, string email, string password)
        {
            Name = name;
            Email = email;
            Password = password;
            Id = id;
        }
    }
}
