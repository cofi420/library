﻿namespace Biblioteka.Models
{
    public class Loan
    {
        public int Id { get; set; }
        public Book Book { get; set; } = new Book();
        public User User { get; set; } = new User();
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Loan() {}
        public Loan(int id, Book book, User user, DateTime start, DateTime end)
        {
            Id = id;
            Book = book;
            User = user;
            Start = start;
            End = end;
        }
    }
}
