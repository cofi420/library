﻿using Biblioteka.Models;
using Microsoft.EntityFrameworkCore;
namespace Biblioteka.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }
        //Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=master;Encrypt=False;Trusted_Connection=True;");
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Loan> Loans{ get; set; }
    }
}
