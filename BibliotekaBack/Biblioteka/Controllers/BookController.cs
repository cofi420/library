﻿using Microsoft.AspNetCore.Mvc;
using Biblioteka.Models;
using Biblioteka.Services.BookService;

namespace Biblioteka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;
        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }
        [HttpGet]
        public async Task<ActionResult<List<Book>>> GetAllBooks()
        {
            return await _bookService.GetAllBooks();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetSingleBook(int id)
        {
            var book = await _bookService.GetSingleBook(id);
            if (book == null)
            {
                return NotFound("Book does not exist");
            }
            return book;
        }
        [HttpPost]
        public async Task<ActionResult<List<Book>>> AddBook(Book book)
        {
            var books = await _bookService.AddBook(book);
            return Ok(books);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<List<Book>>> UpdateBook(int id, Book req)
        {
            var books = await _bookService.UpdateBook(id, req);

            if (books is null)
            {
                return NotFound("Book does not exist");
            }
            return Ok(books);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteBook(int id)
        {
            var isOk = await _bookService.DeleteBook(id);
            if (isOk)
            {
                return NoContent();
            }
            return NotFound("Book does not exist");
        }

    }
}
