﻿using Biblioteka.Models;
using Biblioteka.Services.UserService;
using Microsoft.AspNetCore.Mvc;

namespace Biblioteka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {   
        private readonly IUserService _userService;
        public UserController(IUserService userService) 
        {
            _userService = userService;
        }
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetAllUsers()
        {
            return await _userService.GetAllUsers();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetSingleUser(int id)
        {
            var user = await _userService.GetSingleUser(id);
            if (user == null)
            {
                return NotFound("User does not exist");
            }
            return user;
        }
        [HttpPost]
        public async Task<ActionResult<List<User>>> AddUser(User User)
        {
            var Users = await _userService.AddUser(User);
            return Ok(Users);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<List<User>>> UpdateUser(int id, User req)
        {
            var Users = await _userService.UpdateUser(id, req); 
            if (Users is null)
            {
                return NotFound("User does not exist");
            }
            return Ok(Users);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            var isOk = await _userService.DeleteUser(id);
            if (isOk)
            {
                return NoContent();
            }
            return NotFound("User does not exist");
        }
    }
}
