﻿using Biblioteka.Models;
using Biblioteka.Services.LoanService;
using Microsoft.AspNetCore.Mvc;

namespace Biblioteka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly ILoanService _loanService;

        public LoanController(ILoanService LoanService)
        {
            _loanService = LoanService;
        }
        [HttpGet]
        public async Task<ActionResult<List<Loan>>> GetAllLoans()
        {
            return await _loanService.GetAllLoans();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Loan>> GetSingleLoan(int id)
        {
            var loan = await _loanService.GetSingleLoan(id);
            if (loan == null)
            {
                return NotFound("Loan does not exist");
            }
            return Ok(loan);
        }
        [HttpPost]
        public async Task<ActionResult<List<Loan>>> AddLoan(Loan Loan)
        {
            var loans = await _loanService.AddLoan(Loan);
            return Ok(loans);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<List<Loan>>> UpdateLoan(int id, Loan req)
        {
            var loans = await _loanService.UpdateLoan(id, req);

            if (loans is null)
            {
                return NotFound("Loan does not exist");
            }
            return Ok(loans);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>>DeleteLoan(int id)
        {
            var isOk = await _loanService.DeleteLoan(id);
            if (isOk)
            {
                return NoContent();
            }
            return NotFound("Loan does not exist");
        }
    }
}
