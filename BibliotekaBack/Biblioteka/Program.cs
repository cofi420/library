using Biblioteka.Data;
using Biblioteka.Services.BookService;
using Biblioteka.Services.LoanService;
using Biblioteka.Services.UserService;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IBookService, BookService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ILoanService, LoanService>();
builder.Services.AddDbContext<DataContext>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
//CONECTION STRING Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
